const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const watch = require('gulp-watch');
const del = require("del");

function compiler() {
    return gulp
        .src('./scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./css/'))
}

function sassWatcher() {
    gulp.watch('./scss/*.scss', gulp.series(compiler))
}

function css() {
    return gulp
        .src("scss/style.scss")
        .pipe(sass())
        .pipe(gulp.dest("./dist/styles/"))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest("./dist/styles/"));
}

function clean() {
    return del(["dist"]);
}

function jquery() {
    return gulp.src(['./node_modules/jquery/dist/jquery.min.js'])
        .pipe(gulp.dest('./dist/js/'))
};

function scripts() {
    return (
        gulp
            .src(["./js/**/*"])
            .pipe(gulp.dest("./dist/js/"))
    );
}
function img() {
    return (
        gulp
            .src(["./images/**/*"])
            .pipe(gulp.dest("./dist/images/"))
    );
}
function html() {
    return (
        gulp
            .src(["index.html"])
            .pipe(gulp.dest("./dist/"))
    );
}
const js = gulp.series(scripts);
const watcher = gulp.series(sassWatcher);
const build = gulp.series(clean, gulp.parallel(css,js,jquery, html, img));

exports.html = html;
exports.css = css;
exports.clean = clean;
exports.js = js;
exports.img = img;
exports.jquery = jquery;
exports.watcher = watcher;
exports.build = build;